package org.tt.rbean;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Config {
    @Bean
    public WebClient webClient(){
        return new WebClient();
    }
}
