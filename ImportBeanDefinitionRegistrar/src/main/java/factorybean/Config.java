package factorybean;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@ComponentScan("factorybean")
@Import(WebClientImportBeanDefinitionRegistrar.class)
public class Config {


    public static void main(String[] args) {
        AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext(Config.class);
        WebClient bean = applicationContext.getBean(WebClient.class);
        System.out.println(bean);
    }
}
