package org.tt.rbean.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.tt.rbean.WebClient;

@RestController
public class TestController {
    @Autowired
    WebClient webClient;


    @GetMapping("/get")
    public String get(){
        return webClient.getName();
    }
}
