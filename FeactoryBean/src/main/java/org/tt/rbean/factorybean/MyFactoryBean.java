package org.tt.rbean.factorybean;

import org.springframework.beans.factory.FactoryBean;
import org.springframework.stereotype.Component;

//@Component
public class MyFactoryBean implements FactoryBean<WebClient> {
    @Override
    public WebClient getObject() throws Exception {
        return new WebClient();
    }

    @Override
    public Class<?> getObjectType() {
        return WebClient.class;
    }
}
