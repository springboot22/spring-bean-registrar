package org.tt.rbean.factorybean;

import javafx.scene.web.WebEngineBuilder;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

@Configuration
public class Main {

    @Bean
    public MyFactoryBean myFactoryBean(){
        return new MyFactoryBean();
    }
    public static void main(String[] args) {
        AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext(Main.class);
        WebClient bean = applicationContext.getBean(WebClient.class);
        System.out.println(bean);
    }
}
