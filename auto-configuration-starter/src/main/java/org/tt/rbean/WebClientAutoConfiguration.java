package org.tt.rbean;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class WebClientAutoConfiguration {

    @Bean
    @ConditionalOnProperty(name = "web.client",havingValue = "true",matchIfMissing = false)
    public WebClient webClient(){
        return new WebClient("Default-Auto");
    }
}
