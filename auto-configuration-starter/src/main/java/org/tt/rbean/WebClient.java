package org.tt.rbean;

public class WebClient {
    private String name;
    public WebClient(){}
    public WebClient(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
